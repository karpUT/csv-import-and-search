import React from 'react';
import { shallow, configure } from 'enzyme';
import { expect } from 'chai';
import Adapter from 'enzyme-adapter-react-16';

import Card from '../components/Card';

configure({ adapter: new Adapter() });

describe('<Card/>', () => {
  it('renders body and header properties', () => {
    const wrapper = shallow(
      <Card header={'this is header'} body={<div>this is body</div>} />
    );
    expect(wrapper.contains('this is header')).to.equal(true);

    expect(
      wrapper.contains(
        <div className='card-body'>
          <div>this is body</div>
        </div>
      )
    ).to.equal(true);
  });
});
