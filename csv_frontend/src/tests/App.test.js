import React from 'react';
import { shallow, configure } from 'enzyme';
import { expect } from 'chai';
import Adapter from 'enzyme-adapter-react-16';

import App from '../App';
import Card from '../components/Card';

configure({ adapter: new Adapter() });

describe('<App/>', () => {
  it('renders two <Card /> components', () => {
    const wrapper = shallow(<App />);
    expect(wrapper.find(Card)).to.have.lengthOf(2);
  });

  it('renders a `.container` class', () => {
    const wrapper = shallow(<App />);
    expect(wrapper.find('.container')).to.have.lengthOf(1);
  });
});
