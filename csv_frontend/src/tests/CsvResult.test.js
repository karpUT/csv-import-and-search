import React from 'react';
import { shallow, configure } from 'enzyme';
import { expect } from 'chai';
import Adapter from 'enzyme-adapter-react-16';
import CsvResult from '../components/CsvResult';

configure({ adapter: new Adapter() });

describe('<CsvResult/>', () => {
  it('renders csv result in a table', () => {
    const result = {
      id: '1',
      name: 'test name',
      age: '18',
      address: 'test address',
      team: 'Yellow'
    };
    const wrapper = shallow(<CsvResult resultRow={result} />);

    const { id, name, age, address, team } = result;
    expect(wrapper.contains(<td>{id}</td>)).to.equal(true);
    expect(wrapper.contains(<td>{name}</td>)).to.equal(true);
    expect(wrapper.contains(<td>{age}</td>)).to.equal(true);
    expect(wrapper.contains(<td>{address}</td>)).to.equal(true);
    expect(wrapper.contains(<td>{team}</td>)).to.equal(true);
    expect(wrapper.contains(<td></td>)).to.equal(false); // we don't want any empty table cells 
  });
});
