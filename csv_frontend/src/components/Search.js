import React, { Component } from 'react';
import axios from 'axios';
import CsvResult from './CsvResult';

export default class Search extends Component {
  state = {
    suggestions: [],
    inputValue: ''
  };

  inputChangeHandler = async e => {
    let value = e.target.value;

    this.setState({
      suggestions: [],
      inputValue: value,
      selectedRow: undefined
    });

    if (value !== '') {
      const res = await axios.post('/search', { query: value });
      this.setState({ suggestions: res.data.results });
    }
  };

  renderSuggestions = suggestions => {
    const result = [];

    for (let i = 0; i < suggestions.length; i++) {
      const row = suggestions[i];
      result.push(
        <li
          onClick={() => this.suggestionSelected(row)}
          className="list-group-item list-group-item-action"
          key={i}
          id={`result-${i}`}
        >
          {row.name}
        </li>
      );
    }
    return <ul className="list-group">{result}</ul>;
  };

  suggestionSelected = row => {
    this.setState({
      suggestions: [],
      inputValue: row.name,
      selectedRow: row
    });
  };

  render() {
    const { inputValue, selectedRow, suggestions } = this.state;
    return (
      <div>
        <div className="input-group">
          <input
            value={inputValue}
            onChange={this.inputChangeHandler}
            type="text"
            className="form-control"
          />
        </div>
        {this.renderSuggestions(suggestions)}
        {selectedRow && <CsvResult resultRow={selectedRow} />}
      </div>
    );
  }
}
