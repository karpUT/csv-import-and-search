import React, { Component } from 'react';
import ProgressBar from './ProgressBar';
import axios from 'axios';

export class FileUpload extends Component {
  state = {
    progress: 0,
    showProgress: false,
    error: null
  };

  getConfig = () => {
    return {
      headers: { 'content-type': 'multipart/form-data' },
      onUploadProgress: progressEvent => {
        this.setState({
          progress: Math.round(
            (progressEvent.loaded / progressEvent.total) * 100
          )
        });
      }
    };
  };

  fileSelectedHandler = event => {
    this.setState({ selectedFile: event.target.files[0] });
  };

  fileUploadHandler = async () => {
    const file = this.state.selectedFile;
    if (file === undefined || file === null) return;

    const fd = new FormData();
    fd.append('file', file, file.name);

    this.setState({
      showProgress: true,
      error: null
    });

    try {
      await axios.post('/import', fd, this.getConfig());
    } catch (err) {
      this.setState({ error: err.response.data.error });
    }

    this.setState({
      showProgress: false,
      progress: 0
    });
  };

  render() {
    const { selectedFile, error, showProgress, progress } = this.state;
    return (
      <div>
        {error && <div className="alert alert-danger">{error}</div>}
        <input
          id="uploadField"
          type="file"
          accept=".csv"
          onChange={this.fileSelectedHandler}
        />
        <button
          id="uploadButton"
          className="btn btn-primary btn-block my-2"
          onClick={this.fileUploadHandler}
          disabled={selectedFile ? false : true}
        >
          Upload
        </button>
        {showProgress && <ProgressBar progress={progress} />}
      </div>
    );
  }
}

export default FileUpload;
