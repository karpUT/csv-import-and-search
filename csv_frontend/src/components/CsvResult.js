import React from 'react';
import PropTypes from 'prop-types';

function CsvResult(props) {
  const { id, name, age, address, team } = props.resultRow;
  return (
    <div>
      <table className='table'>
        <thead>
          <tr>
            <th scope='col'>Key</th>
            <th scope='col'>Value</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>ID</td>
            <td>{id}</td>
          </tr>
          <tr>
            <td>Name</td>
            <td>{name}</td>
          </tr>
          <tr>
            <td>Age</td>
            <td>{age}</td>
          </tr>
          <tr>
            <td>Address</td>
            <td>{address}</td>
          </tr>
          <tr>
            <td>team</td>
            <td>{team}</td>
          </tr>
        </tbody>
      </table>
    </div>
  );
}

CsvResult.propTypes = {
  row: PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    age: PropTypes.string.isRequired,
    address: PropTypes.string.isRequired,
    team: PropTypes.string.isRequired
  })
};

export default CsvResult;
