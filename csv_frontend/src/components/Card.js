import React from 'react';
import PropTypes from 'prop-types';

function Card(props) {
  return (
    <div className="card my-4">
      <div className="card-header text-white bg-dark mb-3">{props.header}</div>
      <div className="card-body">{props.body}</div>
    </div>
  );
}

Card.propTypes = {
  header: PropTypes.any.isRequired,
  body: PropTypes.any.isRequired
};

export default Card;
