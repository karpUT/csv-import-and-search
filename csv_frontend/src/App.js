import React, { Component } from 'react';
import axios from 'axios';
import FileUpload from './components/FileUpload';
import Card from './components/Card';
import Search from './components/Search';

axios.defaults.baseURL = 'http://localhost:5000';
class App extends Component {
  render() {
    return (
      <div className="container my-5">
        <Card header={'Upload csv File'} body={<FileUpload />} />
        <Card header={'Search csv Content'} body={<Search />} />
      </div>
    );
  }
}

export default App;
