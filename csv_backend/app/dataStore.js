let csvRows = [];
let searchCache = {};

exports.setCsvRows = value => {
  csvRows = value;
};

exports.getCsvRows = () => {
  return csvRows;
};

exports.getFromSearchCache = key => {
  if (searchCache.hasOwnProperty(key)) {
    return searchCache[key];
  } else {
    return null;
  }
};

exports.addToSearchCache = (key, value) => {
  searchCache[key] = value;
};

exports.clearSearchCache = () => {
  searchCache = {};
};
