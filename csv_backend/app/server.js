const express = require('express');
const bodyParser = require('body-parser');
const csvImport = require('./routes/import');
const csvSearch = require('./routes/search');

const cors = require('cors');
const app = express();
const port = process.env.PORT || 5000;

app.use(cors());
app.use(bodyParser.json());

app.use('/import', csvImport);
app.use('/search', csvSearch);



app.listen(port, () => {
  console.log('server started at port ' + port);
});
