const express = require('express');
const dataStore = require('../dataStore');
const app = express();

app.post('', (req, res) => {
  let query = req.body.query.toLowerCase();
  let results = dataStore.getFromSearchCache(query);

  if (results === null) {
    results = getSearchResults(query, dataStore.getCsvRows());
    dataStore.addToSearchCache(query, results);
  }
  res.status(200).send({
    results: results
  });
  
});

const getSearchResults = (query, rows) => {
  let results = [];

  for (let i = 0; i < rows.length; i++) {
    const row = rows[i];
    if (row.name.toLowerCase().includes(query)) {
      results.push(row);
      if (results.length == 20) {
        return results;
      }
    }
  }
  return results;
};

module.exports = app;
