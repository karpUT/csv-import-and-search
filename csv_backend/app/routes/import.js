const express = require('express');
const multer = require('multer');
const csv = require('fast-csv');
const fs = require('fs');
const dataStore = require('../dataStore');

const app = express();

const fileUpload = multer({
  dest: '../uploads/'
});

app.post('/', fileUpload.single('file'), (req, res) => {
  const file = req.file;
  const csvRows = [];

  if (!file) {
    res.status(422).send({ error: 'No file attached'});
    return;
  }

  csv
    .fromPath(file.path, {
      headers: ['id', 'name', 'age', 'address', 'team']
    })
    .on('data', data => {
      csvRows.push(data);
    })
    .on('end', () => {
      dataStore.clearSearchCache();
      dataStore.setCsvRows(csvRows);
      fs.unlinkSync(file.path);
      res.status(200).send({ status: 'successful' });
    })
    .on('error', () => {
      res.status(422).send({ error: 'failed to process file' });
    });
});

module.exports = app;
