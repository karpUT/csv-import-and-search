# Setup

## Repository
To download the repository use the following command:
```sh
$ git clone https://karpUT@bitbucket.org/karpUT/csv-import-and-search.git
```

## Backend

The backend server uses port 5000 to run the application and has two scripts set up.

> To run the server in a regular node environment use the following commands:
```sh
$ cd csv_backend
$ npm install
$ npm start
```
> To run the server in a docker container you can use:
```sh
$ cd csv_backend
$ npm run start-docker
```
## Frontend

The react front end uses port 3000 to run
>To start the front end use the following commands:
```sh
$ cd csv_frontend
$ npm install
$ npm start
```
